import Immutable from 'immutable';
import moment from 'moment';

const GET_USER = 'user/GET_USER';
const GET_USER_SUCCESS = 'user/GET_USER_SUCCESS';
const GET_USER_FAIL = 'user/GET_USER_FAIL';

const ADD_EDIT_USER = 'user/ADD_EDIT_USER';
const ADD_EDIT_USER_SUCCESS = 'user/ADD_EDIT_USER_SUCCESS';
const ADD_EDIT_USER_FAIL = 'user/ADD_EDIT_USER_FAIL';

const DELETE_USER = 'user/DELETE_USER';
const DELETE_USER_SUCCESS = 'user/DELETE_USER_SUCCESS';
const DELETE_USER_FAIL = 'user/DELETE_USER_FAIL';

const EDIT_USER = 'user/EDIT_USER';
const defaultData = { id: 1, firstName: 'Sunny', lastName: 'S.M', email: 'attwal.sunny@gmail.com', status: 'active', role: 10, created: moment() };
const initialState = Immutable.fromJS({
  userBusy: false,
  userError: null,
  users: [],
  user: {}
});

export default function reducer (state = initialState, action) {
  switch(action.type) {
    case GET_USER:
      return state
        .set('userBusy', true)
        .set('userError', null);
    case GET_USER_SUCCESS: {
      return state
        .set('userBusy', false)
        .set('users', action.res);
    }
    case GET_USER_FAIL:
      return state
        .set('userBusy', false)
        .set('userError', 'Something went wrong');
    case ADD_EDIT_USER:
      return state
        .set('userBusy', true)
        .set('userError', null);
    case ADD_EDIT_USER_SUCCESS: {
      const users = state.get('users');
      const user = action.res;
      const index = users.findIndex((obj => obj.id === user.id));
      if (index >= 0) {
        users[index].firstName = user.firstName;
        users[index].lastName = user.lastName;
        users[index].email = user.email;
        users[index].role = user.role;
        users[index].status = user.status;
      } else {
        users.unshift(action.res);
      }
      return state
      .set('userBusy', false)
      .set('users', users)
      .set('user', {});
    }
    case ADD_EDIT_USER_FAIL:
      return state
        .set('userBusy', false)
        .set('userError', 'Something went wrong');
    case DELETE_USER:
      return state
        .set('userBusy', true);
    case DELETE_USER_SUCCESS: {
      const users = state.get('users');
      const index = users.indexOf(action.user);
      if (index !== -1) {
        users.splice(index, 1);
      }
      return state
        .set('userBusy', false)
        .set('users', users)
        .set('user', {});
    }
    case DELETE_USER_FAIL:
      return state
        .set('userBusy', true)
        .set('userError', 'Something went wrong');
    case EDIT_USER:
      return state
        .set('user', action.user);
    default:
      return state;
  }
}

export const getUsers = () => async (dispatch, getState) => {
    dispatch({ type: GET_USER });
    const res = [];
    res.unshift(defaultData);
  try {
    dispatch({ type: GET_USER_SUCCESS, res });
  } catch (err) {
    dispatch({ type: GET_USER_FAIL });
  }
};

export const addUser = (formData) => async (dispatch, getState) => {
  dispatch({ type: ADD_EDIT_USER });
  try {
    dispatch({ type: ADD_EDIT_USER_SUCCESS, res: formData });
    return true;
  } catch (err) {
    dispatch({ type: ADD_EDIT_USER_FAIL });
    return false;
  }
};

export const editUser = user => async (dispatch, getState) => {
  dispatch({ type: EDIT_USER, user });
};

export const updateUser = (formData) => async (dispatch, getState) => {
  dispatch({ type: ADD_EDIT_USER });
  try {
    dispatch({ type: ADD_EDIT_USER_SUCCESS, res: formData });
    return true;
  } catch (err) {
    dispatch({ type: ADD_EDIT_USER_FAIL });
    return false;
  }
};

export const deleteUser = (user) => async (dispatch, getState ) => {
  dispatch({ type: DELETE_USER });
  try {
    dispatch({ type: DELETE_USER_SUCCESS, user });
    return user;
  } catch (err) {
    dispatch({ type: DELETE_USER_FAIL });
    return false;
  }
};
