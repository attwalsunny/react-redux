import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Formik } from "formik";
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import * as Yup from 'yup';
import { Button, Modal, Form, Col } from 'react-bootstrap';
import { addUser, updateUser } from '../../redux/modules/user';
import Spinner from '../Spinner';


const userSchema = Yup.object().shape({
  firstName: Yup.string()
    .required("First name is required"),
  lastName: Yup.string()
    .required("Last name is required"),
  email: Yup.string()
    .required("Email is required")
    .email("Email has to be valid"),
  role: Yup.string()
    .required("Role is required"),
  status: Yup.string()
    .required("Status is required"),
});

const AddUserForm = (props) => {
  const { userBusy, show, size, handleAddUser, user } = props;
  return (
    <Modal
      size={size}
      className="addUerDialog"
      show={show}
      onHide={() => {
        handleAddUser(null);
      }}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <Formik
          initialValues={{
            firstName: user.firstName || '',
            lastName: user.lastName || '',
            email: user.email || '',
            role: user.role || '',
            status: user.status || ''
          }}
          validationSchema={userSchema}
          onSubmit={async (values) => {
            const { dispatch, users, user } = props;
            if (user && user.id) {
              const formData = Object.assign(values, {
                id: user.id
              });
              await dispatch(updateUser(formData));
              handleAddUser(null);

            } else {
              const ids = users.map(item => item.id);
              const id = Math.max([...ids])
              const formData = Object.assign(values, {
                id: (id + 1),
                created: moment()
              });
              await dispatch(addUser(formData));
              handleAddUser(null);
            }
          }}
        >
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            touched,
            isValid,
            errors,
            isSubmitting
          }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Control
                    type="text"
                    placeholder="First name"
                    name="firstName"
                    value={values.firstName}
                    onChange={handleChange}
                    isValid={touched.firstName && !errors.firstName}
                  />
                </Form.Group>
                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Control
                    type="text"
                    placeholder="Last name"
                    name="lastName"
                    value={values.lastName}
                    onChange={handleChange}
                    isValid={touched.lastName && !errors.lastName}
                  />
                </Form.Group>
              </Form.Row>
              <Form.Group controlId="formGridAddress1">
                <Form.Control
                  type="email"
                  placeholder="example@abc.com"
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  isValid={touched.email && !errors.email}
                />
              </Form.Group>
              <Form.Row>
                <Form.Group as={Col} controlId="formGridEmail">
                  <Form.Control
                    as="select"
                    name="role"
                    value={values.role}
                    onChange={handleChange}
                    isValid={touched.role && !errors.role}
                  >
                    <option value="">Select Role</option>
                    <option value="10">Super Admin</option>
                    <option value="20">Admin</option>
                  </Form.Control>
                </Form.Group>
                <Form.Group as={Col} controlId="formGridPassword">
                  <Form.Control
                    as="select"
                    name="status"
                    value={values.status}
                    onChange={handleChange}
                    isValid={touched.status && !errors.status}
                  >
                    <option value="">Select Status</option>
                    <option value="active">Active</option>
                    <option value="inactive">Inactive</option>
                  </Form.Control>
                </Form.Group>
              </Form.Row>
              <div className="userBtn">
                <Button
                  type="submit"
                  size="sm"
                  disabled={isSubmitting || !isValid || !isEmpty(errors)}
                >
                  {userBusy && <Spinner />}
                  Submit
                </Button>
                <Button size="sm" onClick={() => handleAddUser(null)}>Close</Button>
              </div>
            </Form>
          )}
        </Formik>
      </Modal.Body>
    </Modal>
  );
}

AddUserForm.propTypes = {
  dispatch: PropTypes.func,
  userBusy: PropTypes.bool,
  user: PropTypes.object,
  show: PropTypes.bool,
  size: PropTypes.string,
  handleAddUser: PropTypes.func
};

AddUserForm.defaultProps = {
  dispatch: null,
  userBusy: false,
  user: {},
  show: false,
  size: "lg",
  handleAddUser: null
}

export default connect(state => {
  return ({
    users: state.get('user').get('users'),
    user: state.get('user').get('user'),
    userBusy: state.get('user').get('userBusy')
  })
})(AddUserForm);


