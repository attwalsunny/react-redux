import React from 'react';
import Spinner from 'react-bootstrap/Spinner';

const Spiner = () => (
  <Spinner
    as="span"
    animation="grow"
    size="sm"
    role="status"
    aria-hidden="true"
  />
);

export default Spiner;