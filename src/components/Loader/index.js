import React from 'react';
import Spinner from 'react-bootstrap/Spinner';
import './Loader.scss';

const Loader = (props) => {
  const {
    size = 'sm',
    animation = 'grow'
  } = props;
  return (
    <div className="loaderContainer">
      <Spinner
        animation={animation}
        size={size}
        variant="primary"
      />
    </div>
  );
}

export default Loader;