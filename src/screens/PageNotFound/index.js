import React from 'react';
import { Link } from "react-router-dom";
import { Container, Row, Col } from 'react-bootstrap';
import '../../styles/main.scss';

const Home = () => {
  return (
    <div>
      <Container fluid>
        <Row className="pageNotFoundContainer">
          <Col className="content" sm={12}>
            <span>Oops requested page not found...</span>
            <Link to="/"> Back </Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Home;
