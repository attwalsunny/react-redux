import { combineReducers } from 'redux-immutable';
import { createBrowserHistory } from 'history';
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { fromJS } from 'immutable';
import { routerMiddleware, connectRouter } from 'connected-react-router/immutable';

import moduleReducers from './moduleReducers';
import ApiClient from './ApiClient';
import clientMiddleware from './clientMiddleware';

const history = createBrowserHistory();

const api = new ApiClient();

export default function configureStore(preLoadedState) {
  const middlewares = [
    thunkMiddleware.withExtraArgument(api),
    clientMiddleware(api),
    routerMiddleware(history)
  ];

  const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const rootReducer = (history) => combineReducers({
    router: connectRouter(history),
    ...moduleReducers
  });

  const initialState = fromJS(preLoadedState || {});
  return createStore(
    rootReducer(history),
    initialState,
    composeEnhancer(
      applyMiddleware(...middlewares)
    )
  );
}
