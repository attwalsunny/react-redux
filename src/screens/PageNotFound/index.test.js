import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, configure } from 'enzyme';
import PageNotFound from './index';

configure({adapter: new Adapter()});
describe('Test cases for PageNotFound components', () => {
  it('renders without crashing', () => {
    shallow(<PageNotFound />);
  });
});