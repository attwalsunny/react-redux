export const capitalize = (str) => {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

export const getStatus = (status) => {
  const temp = parseInt(status, 10);
  if (temp === 10) return 'Super Admin';
  else if (temp === 20) return 'Admin';
  else return 'Guest';
}