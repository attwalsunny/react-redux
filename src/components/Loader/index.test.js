import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Loader from './index';

const clickFn = jest.fn();
configure({adapter: new Adapter()});

describe('Test cases for loader component', () => {
  it('renders without crashing', () => {
    shallow(<Loader />);
  });
});