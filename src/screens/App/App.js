import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { ConnectedRouter } from 'connected-react-router/immutable';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Loader from '../../components/Loader';

const AppScreen = React.lazy(() => import('../Home'));
const PageNotFoundScreen = React.lazy(() => import('../PageNotFound'));
const UserScreen = React.lazy(() => import('../User'));

const App = (props) => {
  const { history } = props;
  return (
    <ConnectedRouter history={history} >
      <BrowserRouter>
        <Switch>
          <Route
            exact
            path="/"
            name="home"
            component={() => (
              <Suspense fallback={<Loader />}>
                <AppScreen />
              </Suspense>
            )}
          />
          <Route
            exact
            path="/user"
            name="user"
            component={() => (
              <Suspense fallback={<Loader />}>
                <UserScreen />
              </Suspense>
            )}
          />
          <Route
            exact
            path="*"
            name="page not found"
            component={() => (
              <Suspense fallback={<Loader />}>
                <PageNotFoundScreen />
              </Suspense>
            )}
          />
        </Switch>
      </BrowserRouter>
    </ConnectedRouter>
  );
}

App.propTypes = {
  history: PropTypes.object
};

export default App;