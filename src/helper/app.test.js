import { capitalize, getStatus}  from './app';

test('capitalize test case', () => {
  expect(capitalize('sunny')).toBe('Sunny');
  expect(capitalize('')).toBe('');
});

test('getStatus test case', () => {
  expect(getStatus(10)).toBe('Super Admin');
  expect(getStatus(20)).toBe('Admin');
  expect(getStatus()).toBe('Guest');
});