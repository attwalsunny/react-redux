import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import Spiner from './index';

const clickFn = jest.fn();
configure({adapter: new Adapter()});

describe('Test cases for Spiner component', () => {
  it('renders without crashing', () => {
    shallow(<Spiner />);
  });
});