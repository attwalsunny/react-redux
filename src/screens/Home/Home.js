import React from 'react';
import { Link } from "react-router-dom";
import Jumbotron from 'react-bootstrap/Jumbotron';
import { Container, Row, Col} from 'react-bootstrap';
import '../../styles/main.scss';

const Home = () => {
  return (
    <div className="pageContainer">
      <Jumbotron fluid>
        <Container>
          <h1>Welcome to React App by Sunny S.M (Fullstack Software Developer)</h1>
          <p>
            This app created using (React, Hooks, Redux, Redux Saga, TDD, SASS, Bootstrap) modules.
          </p>
        </Container>
      </Jumbotron>
      <Container fluid>
        <Row className="pageNotFoundContainer">
          <Col className="content" sm={12}>
            <Link to="/user"> Go to user section </Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Home;
