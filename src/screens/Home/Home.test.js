import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, mount, configure } from 'enzyme';
import Home from './Home';

configure({adapter: new Adapter()});
describe('Test cases for Home components', () => {
  it('renders without crashing', () => {
    shallow(<Home />);
  });
});