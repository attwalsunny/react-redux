import React from 'react';
import moment from 'moment';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, Container, Row, Col, Table} from 'react-bootstrap';
import DeleteModel from '../../components/models/deleteConfirm';
import AddUserForm from '../../components/models/addUserModel';
import { getUsers, editUser } from '../../redux/modules/user';
import { capitalize, getStatus } from '../../helper/app';
import {ReactComponent as AddIcon} from '../../styles/images/iconAdd.svg';
import {ReactComponent as DeleteIcon} from '../../styles/images/iconDelete.svg';
import '../../styles/main.scss';

const User = (props) => {
  const [deleteItem, setDeleteItem] = React.useState(null);
  const [addEditUser, setAddEditUser] = React.useState(null);
  const { dispatch, users } = props;

  React.useEffect(() => {
    dispatch(getUsers());
  }, [dispatch])
  
  const getHtml = () => {
    const result = [];
    if (users && users.length) {
      users.forEach(item => {
        result.push(
          <tr key={item.id}>
            <td>{`${capitalize(item.firstName)}  ${capitalize(item.lastName)}`}</td>
            <td>{item.email || ''}</td>
            <td>{getStatus(item.role)}</td>
            <td>{capitalize(item.status)}</td>
            <td>{moment(item.created).format('YYYY-MM-DD H:m A')}</td>
            <td>
              <span
                className="cursor"
                onClick={() => {
                  dispatch(editUser(item));
                  handleAddUser(item)
                }}
              >
                <AddIcon />
              </span>
              <span
                className="cursor marginLeft10"
                onClick={() => {
                  dispatch(editUser(item));
                  handleUserDelete(item)
                }}
              >
                <DeleteIcon />
              </span>
            </td>
          </tr>
        )
      });
    }
    return result;
  }

  const handleUserDelete = (user) => {
    console.log('STATUS', user);
    if (!user) {
      dispatch(editUser({}));
    }
    setDeleteItem(user)
  }

  const handleAddUser = (user) => {
    if (!user) {
      dispatch(editUser({}));
    }
    setAddEditUser(user);
  }

  return (
    <div className="userPageContainer">
      <Container fluid>
        <Row className="justify-content-md-left">
          <Col xs lg="2">
            <Link to="/">Home</Link>
            {' > '}
            User list
          </Col>
        </Row>
        <Row className="margin15">
          <Col className="displyFlex">
            <input className="searchBox" name="searchBox" placeholder="Search" />
            <Button className="addBtn" size="sm" variant="secondary" onClick={() => setAddEditUser(true)}>Add User</Button>
          </Col>
        </Row>
        <Row>
          <Table className="tableContent" responsive hover bordered striped>
            <thead>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th>Status</th>
                <th>Created</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>{getHtml()}</tbody>
          </Table>
        </Row>
      </Container>
      {deleteItem && deleteItem.id && (
        <DeleteModel
          show={(deleteItem.id > 0) ? true : false}
          handleDelete={(delObj) => {
            setDeleteItem(delObj);
          }}
        />
      )}
      {addEditUser && (
        <AddUserForm
          show={addEditUser ? true : false}
          handleAddUser={handleAddUser}
        />
      )}
    </div>
  );
};

User.propTypes = {
  dispatch: PropTypes.func,
  users: PropTypes.any,
};

User.defaultProps = {
  dispatch: null,
  users: [],
}

export default connect(state => {
  return ({
    users: state.get('user').get('users')
  })
})(User);
