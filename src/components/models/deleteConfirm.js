import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Modal } from 'react-bootstrap';
import { deleteUser, editUser } from '../../redux/modules/user';

const DeleteModel = (props) => {
  const {
    show,
    size,
    handleDelete,
    user,
    dispatch
  } = props;

  return (
    <Modal
      size={size}
      className="deleteDialog"
      show={show}
      onHide={() => handleDelete(null)}
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Delete confirm dialog
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        Are you sure you want to delete this record
      </Modal.Body>
      <Modal.Footer>
        <Button
          size="sm"
          onClick={() => {
            dispatch(editUser({}));
            handleDelete(null)
          }}
        >
          Cancel
        </Button>
        <Button
          size="sm"
          onClick={() => {
            dispatch(deleteUser(user));
            handleDelete(true);
          }}
        >
          Sure
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

DeleteModel.propTypes = {
  dispatch: PropTypes.func,
  userBusy: PropTypes.bool,
  show: PropTypes.bool,
  size: PropTypes.string,
  handleAddUser: PropTypes.func
};

DeleteModel.defaultProps = {
  dispatch: null,
  userBusy: false,
  show: false,
  size: "sm",
  handleAddUser: null
}

export default connect(state => {
  return ({
    user: state.get('user').get('user')
  })
})(DeleteModel);